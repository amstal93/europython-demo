EuroPython Demo
===============

Demo for the talk on Modern Continuous Delivery

Getting Started
---------------

To start developing on this project simply bring up the Docker setup:

.. code-block:: console

    docker-compose up --build -d
    docker-compose exec application python manage.py migrate
    docker-compose logs -f

Open your web browser at http://localhost:8000 to see the application
you're developing.  Log output will be displayed in the terminal, as usual.

Initial Setup (APPUiO + GitLab)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Create a *production*, *integration* and *development* project at the
   `VSHN Control Panel <https://control.vshn.net/openshift/projects/appuio%20public>`_.
   For quota sizing consider roughly the sum of ``limits`` of all
   resources (must be strictly greater than the sum of ``requests``):

   .. code-block:: console

        $ grep -A2 limits deployment/*yaml
        $ grep -A2 requests deployment/*yaml

#. Create a service account as described in the `APPUiO docs
   <https://appuio-community-documentation.readthedocs.io/en/latest/services/webserver/50_pushing_to_appuio.html>`_:

   Create a service account, grant permissions to push images and apply
   configurations, and get the service account's token value:

   .. code-block:: console

        $ oc -n europython-demo-production create sa gitlab-ci
        $ oc -n europython-demo-production policy add-role-to-user edit -z gitlab-ci
        $ oc -n europython-demo-production sa get-token gitlab-ci

#. Configure the Kubernetes integration in your GitLab project adding
   the ``token`` value from the ``gitlab-ci-token`` secret to:

   -  Operations > Kubernetes > "APPUiO" > Kubernetes cluster details > Service Token

   (*Note:* Make sure "GitLab-managed cluster" is unchecked in the cluster details.)

#. Grant the service account permissions on the *development* and *integration*
   projects:

   .. code-block:: console

        $ oc -n europython-demo-integration policy add-role-to-user \
          edit system:serviceaccount:europython-demo-production:gitlab-ci
        $ oc -n europython-demo-development policy add-role-to-user \
          edit system:serviceaccount:europython-demo-production:gitlab-ci

Working with Docker
^^^^^^^^^^^^^^^^^^^

Create/destroy development environment:

.. code-block:: console

    docker-compose up -d    # create and start; omit -d to see log output
    docker-compose down     # docker-compose kill && docker-compose rm -af

Start/stop development environment:

.. code-block:: console

    docker-compose start    # resume after 'stop'
    docker-compose stop     # stop containers, but keep them intact

Other useful commands:

.. code-block:: console

    docker-compose ps       # list running containers
    docker-compose logs -f  # view (and follow) container logs

See the `docker-compose CLI reference`_ for other commands.

.. _docker-compose CLI reference: https://docs.docker.com/compose/reference/overview/

CI/CD Process
^^^^^^^^^^^^^

We have 3 environments corresponding to 3 namespaces on our container
platform: *development*, *integration*, *production*

- Any merge request triggers a deployment (of the feature branch) on
  *development*.
- Any change on the main branch, e.g. when a merge request is merged into
  ``master``, triggers a deployment on *integration*.
- To trigger a deployment on *production* push a Git tag, e.g.

  .. code-block:: console

    $ git checkout master
    $ git tag 1.0.0
    $ git push --tags
